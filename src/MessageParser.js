const Discord = require('discord.js');

module.exports = function (message, prefix) {
    if (!message.content.trim().startsWith(prefix)) return;
    let args = message.content
        .trim()
        .substring(prefix.length)
        .trim()
        .match(/"(?:\\"|\\\\|[^"])*"|\S+/g); // match all strings between spaces that are not in quotes
    if ((args || []).length >= 1) {
        args = args.map(
            (arg) =>
                (arg || '')
                    .replace(/^"(.*)"$/gm, '$1') // remove unwanted quotes ("abc" -> abc)
                    .replace(/(?<!(?<!\\)\\)\\"/gm, '"') // remove backslash before quotes (\" -> ")
                    .replace(/\\\\/gm, '\\'), // replace escaped quotes to normal (\\ -> \)
        );
        let command = args.shift();
        args = args.map((arg) => {
            let match;

            if ((match = arg.match(new RegExp(Discord.MessageMentions.USERS_PATTERN.source))))
                return message.mentions.users.get(match[1]) || arg;
            if ((match = arg.match(new RegExp(Discord.MessageMentions.CHANNELS_PATTERN.source))))
                return message.mentions.channels.get(match[1]) || arg;
            if ((match = arg.match(new RegExp(Discord.MessageMentions.ROLES_PATTERN.source))))
                return message.mentions.roles.get(match[1]) || arg;

            if (!isNaN(+arg)) return parseInt(arg);
            return arg;
        });

        return { command, args };
    }
};
