const Guild = require('../models/Guild');

/**
 * @namespace DatabaseService
 */
const DatabaseService = {
    async getGuildById(id) {
        return Guild.findOne({ _id: id });
    },

    /**
     * @param {Number} id
     * @returns {Promise<Guild>}
     */
    async addGuild(id) {
        const guild = new Guild({ _id: id });
        await guild.save();
        return guild;
    },
};

module.exports = DatabaseService;
