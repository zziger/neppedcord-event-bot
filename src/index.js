const Discord = require('discord.js');
const chalk = require('chalk');
const fs = require('fs');
const path = require('path');
const DatabaseService = require('./services/DatabaseService');
const config = require('../config');
const mongoose = require('mongoose');
const messageParser = require('./MessageParser');
const messageAnswer = require('./MessageAnswer');
const CommandError = require('./errors/CommandError');

class Client extends Discord.Client {
    constructor() {
        super();
        this.store = {
            commands: [],
        };
        this.on('ready', this.onReady.bind(this));
    }

    async onReady() {
        await mongoose.connect(config.mongo, { useNewUrlParser: true, useUnifiedTopology: true });
        await this.registerCommands();
        console.log(this.store.commands);
        this.on('message', (m) => this.onMessage(m));
        console.log(chalk.green`Bot started successfully`);
    }

    async onMessage(message) {
        let guildObject = null;
        if (message.channel.type === 'text') {
            guildObject = await DatabaseService.getGuildById(message.guild.id);
            if (!guildObject) guildObject = await DatabaseService.addGuild(message.guild.id);
        }

        if (message.author.bot && (!guildObject || !guildObject.triggerBots)) return;
        const prefix = guildObject && guildObject.prefix ? guildObject.prefix : config.defaultPrefix;
        if (!message.content.trim().startsWith(prefix)) return;

        let info;
        let commandObj;
        try {
            info = messageParser(message, prefix);

            commandObj = this.store.commands.find((c) => {
                if (typeof c.trigger === 'string') return info.command === c.trigger;
                if (c.trigger instanceof RegExp) return info.command.match(c.trigger);
                if (Array.isArray(c.trigger)) {
                    c.trigger.any((trigger) => {
                        if (typeof trigger === 'string') return info.command === c.trigger;
                        if (trigger instanceof RegExp) return trigger.match(info.command);
                    });
                }
            });
        } catch (e) {
            return message.reply('нет такой команды, пасаси))0');
        }

        try {
            const response = await commandObj.run(message, info.args, info.command);
            const answer = messageAnswer.resolveAnswer(await response);
            if (answer) message.channel.send(answer);
        } catch (e) {
            if (e instanceof CommandError) return message.channel.send(e.answer);
            console.error(e);
            await message.channel.send(
                new CommandError(
                    'ошибочка вышла, такая, примерно межгалактического масштаба. \n' +
                        'крч, пж, передай овнеру бота что он ебланоид, спс',
                ).answer,
            );
        }
    }

    async registerCommands() {
        const files = await fs.promises.readdir(path.join(__dirname, 'commands'));
        for (const file of files) {
            try {
                const command = require(path.join(__dirname, 'commands', file));
                const commandInstance = new command();
                const info = commandInstance.info;
                if (!info || !info.name) throw new Error('no_info');
                this.store.commands.push({
                    ...info,
                    run: commandInstance.run,
                });
                console.log(`Registered ${info.name} command`);
            } catch (e) {
                console.error(chalk.red`Failed to register ${file} command. Error details:`);
                console.error(e);
            }
        }
    }
}

const client = new Client();
client.login(process.env.TOKEN).catch(console.error);
