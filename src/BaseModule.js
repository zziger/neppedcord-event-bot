const Discord = require('discord.js');

class BaseModule {
    /**
     * @returns {Object}
     */
    register() {}
}

module.exports = BaseModule;
