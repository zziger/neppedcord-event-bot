const mongoose = require('mongoose');

const guildSchema = new mongoose.Schema({
    _id: String,
    description: String,
    xp: Number,
    guilds: {
        type: Map,
        of: {
            xp: Number,
        },
    },
    banned: Boolean,
});

module.exports = mongoose.model('User', guildSchema);
