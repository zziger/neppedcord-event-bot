const mongoose = require('mongoose');

const guildSchema = new mongoose.Schema({
    _id: String,
    prefix: String,
    triggerBots: { type: Boolean, default: false },
    disabledCommands: [String],
    blocked: Boolean,
});

module.exports = mongoose.model('Guild', guildSchema);
