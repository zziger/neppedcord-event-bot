const BaseCommand = require('../BaseCommand');
const CommandError = require('../errors/CommandError');

class TestCommand extends BaseCommand {
    constructor() {
        super();
        this.info = {
            name: 'test',
            trigger: /^t[ae]st$/gi,
            shortDescription: 'хуй??',
            description: 'команда для ебланов крч, поебать',
            usage: '(цифра блеать)',
            category: 'hui',
            aliases: ['test', 'tast'],
        };
    }

    run(message, args) {
        if (typeof args[0] !== 'number') throw new CommandError('где цифра первым аргументом, блеать?');
        return 'АХАХХАХА ТЫ НАПИСАЛ ' + args[0];
    }
}

module.exports = TestCommand;
