const messageAnswer = require('../MessageAnswer');

class CommandError extends Error {
    constructor(desc, title) {
        super(desc);
        this.answer = messageAnswer.error(desc, title);
    }
}

module.exports = CommandError;
