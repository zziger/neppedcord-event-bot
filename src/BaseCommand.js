const Discord = require('discord.js');

/**
 * @typedef {object} CommandInfo
 * @property {string} name
 * @property {(string|RegExp|string[]|RegExp[])} [trigger]
 * @property {Permissions} [permissions]
 * @property {string} [shortDescription]
 * @property {string} [description]
 * @property {string} [usage]
 * @property {string} [category]
 * @property {string[]} [aliases]
 */

/**
 * @abstract
 * @property {CommandInfo} info
 */
class BaseCommand {
    /**
     * @property {Message} message
     * @property {(string|GuildMember|User|Channel|Role)[]} args
     * @property {string} command
     */
    async run(message, args, command) {}
}

module.exports = BaseCommand;
