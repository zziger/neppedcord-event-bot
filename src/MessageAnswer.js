const Discord = require('discord.js');

module.exports = {
    resolveAnswer(answer) {
        if (typeof answer === 'string' || typeof answer === 'number' || answer instanceof Discord.MessageEmbed)
            return answer;
        if (answer && 'type' in answer)
            switch (answer.type) {
                case 'error':
                    return this.error(answer.message || '', answer.title, answer.custom);
                case 'info':
                default:
                    return this.info(answer.message || '', answer.title, answer.custom);
            }
    },

    info(message, title, custom) {
        return new Discord.MessageEmbed({
            color: '#2f3036',
            title,
            description: message,
            ...custom,
        });
    },

    error(message, title, custom) {
        return new Discord.MessageEmbed({
            color: '#ff0000',
            title: title || 'Произошла ошибка',
            description: message,
            ...custom,
        });
    },
};
